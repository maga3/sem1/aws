package by.andruhovich.request;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;

public class MultiThreadedExample {

    public MultiThreadedExample() {
        super();
    }

    public static void main(String[] args) {
        HttpClient httpClient = new HttpClient(new MultiThreadedHttpConnectionManager());

        GetThread[] threads = new GetThread[10000];
        for (int i = 0; i < 10000; i++) {
            GetMethod get = new GetMethod("https://rjqjz9c7wf.execute-api.us-east-2.amazonaws.com/default/arraysSum");
            get.setFollowRedirects(true);
            threads[i] = new GetThread(httpClient, get, i + 1);
        }

        for (GetThread thread : threads) {
            thread.start();
        }

    }

    /**
     * A thread that performs a GET.
     */
    static class GetThread extends Thread {

        private HttpClient httpClient;
        private GetMethod method;
        private int id;

        public GetThread(HttpClient httpClient, GetMethod method, int id) {
            this.httpClient = httpClient;
            this.method = method;
            this.id = id;
        }

        /**
         * Executes the GetMethod and prints some satus information.
         */
        public void run() {

            try {

                System.out.println(id + " - about to get something from " + method.getURI());
                httpClient.executeMethod(method);

                System.out.println(id + " - get executed");
                byte[] bytes = method.getResponseBody();

                System.out.println(id + " - " + bytes.length + " bytes read");

            } catch (Exception e) {
                System.out.println(id + " - error: " + e);
            } finally {
                method.releaseConnection();
                System.out.println(id + " - connection released");
            }
        }

    }

}
